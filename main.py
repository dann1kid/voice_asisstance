import pyaudio
import json
from vosk import Model, KaldiRecognizer
import pyautogui
from monitorcontrol import get_monitors

# setup
model = Model(r"small")  # полный путь к модели
rec = KaldiRecognizer(model, 16000)
p = pyaudio.PyAudio()
stream = p.open(
    format=pyaudio.paInt16,
    channels=1,
    rate=16000,
    input=True,
    frames_per_buffer=16000
)


def listen():
    while True:
        data = stream.read(5000, exception_on_overflow=False)
        try:
            answer = json.loads(rec.Result()) if rec.AcceptWaveform(data) else json.loads(rec.PartialResult())
        except Exception:
            raise
        else:
            try:
                yield answer['text']
            except KeyError:
                pass


def change_lux(monitor, vol):
    with monitor:
        try:
            luminance = monitor.get_luminance() + vol
            if luminance >= 100:
                luminance = 100
            monitor.set_luminance(luminance)
        except Exception:
            raise


def change_power_mode(monitor, mode):
    with monitor:
        try:
            monitor.set_power_mode(mode)
        except Exception as e:
            print(f"CANT CHANGE POWER MODE!: {e}")
        return None


def main():
    # обозначим мониторы
    current_monitors = [x for x in get_monitors()]
    # аудио поток
    stream.start_stream()
    # TODO: добавить телеграм бота для удаленных команд через воисдекодер

    # команды
    # TODO: переделать на что-то более вразумительное
    # нужно угадывать вектор действий
    # Лемматизация через pymystem3
    # Попробовать собрать датасет с командами и прогнать через tensorflow
    #

    for _ in listen():
        print(_)
        if "следующий трек" in _:
            pyautogui.press('nexttrack')
        elif "погромче" in _:
            pyautogui.press('volumeup')
        elif "меньше яркость" in _:
            for monitor in current_monitors:
                change_lux(monitor, -10)
        elif "больше яркость" in _:
            for monitor in current_monitors:
                change_lux(monitor, +10)
        elif "максимальная яркость" in _:
            for monitor in current_monitors:
                change_lux(monitor, +100)

        elif "выключи монитор" in _:
            for monitor in current_monitors:
                change_power_mode(monitor, 4)

        elif "включи монитор" in _:
            for monitor in current_monitors:
                change_power_mode(monitor, 1)


if __name__ == "__main__":
    main()
